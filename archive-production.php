<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, ACSR already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<?php
$which_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
$number_of_highlighted_posts = 8;

/* HOW TO SORT */

// These are the default options
// correspond to url /production/
$args = array(  'posts_per_page' => 12,
                'paged' => $which_page,
                'post_type' => 'production',
                'orderby' => 'meta_value_num',
                'meta_key' => 'ID wpcf-annee',
                'order' => 'DESC');

/*  We allow to specify a different order through the url, i.e.
 *
 *  /production/?order=ASC
 */

if (array_key_exists('order', $_GET))  {
    $args['order'] = $_GET['order'];
}

/*  We also allow to specify a different column to order by,
 *  and in case of ordering by a meta value, which one:
 *
 *  /production/?orderby=title
 *  /production/?orderby=title&order=ASC
 *  /production/?orderby=meta_value&meta_key=wpcf-genre&order=ASC
 *
 */

if ( array_key_exists('orderby', $_GET) )  {
    $args['orderby'] = $_GET['orderby'];
    if ( $_GET['orderby'] == 'meta_value' && array_key_exists('meta_key', $_GET))  {
        $args['meta_key'] = $_GET['meta_key'];
    }
}

if ($args['orderby'] != 'meta_value') {
    unset( $args['meta_key'] );
}

query_posts($args);


/* HOW TO DISPLAY */

$format = 'detail';

if ( array_key_exists('format', $_GET) )  {
    $format = $_GET['format'];
}

?>
    <section id="primary" class="site-content">
        <div id="content" role="main">

        <?php if ( have_posts() ) : ?>

<article class="post">
            <header class="archive-header">
                <h1 class="productions-achevees"><?php
                    if ( is_day() ) :
                        printf( __( 'Daily Archives: %s', 'acsr' ), '<span>' . get_the_date() . '</span>' );
                    elseif ( is_month() ) :
                        printf( __( 'Monthly Archives: %s', 'acsr' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'acsr' ) ) . '</span>' );
                    elseif ( is_year() ) :
                        printf( __( 'Yearly Archives: %s', 'acsr' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'acsr' ) ) . '</span>' );
                    else :
                        _e( 'Phonotèque', 'acsr' );?>

                    <?php endif;
                ?></h1>

            </header><!-- .archive-header -->

<?php $args = array(
    'show_option_all'    => 'ou sélectionner une thématique  &nbsp;&nbsp;&nbsp;  &rarr;',
    'show_option_none'   => '',
    'orderby'            => 'ID',
    'order'              => 'ASC',
    'show_count'         => 0,
    'hide_empty'         => 1,
    'child_of'           => 0,
    'exclude_tree'         => '20',
    'exclude'          => '1',
    'echo'               => 1,
    'selected'           => 0,
    'hierarchical'       => 0,
    'name'               => 'cat',
    'id'                 => '',
    'class'              => 'postform',
    'depth'              => 0,
    'tab_index'          => 0,
    'taxonomy'           => 'category',
    'hide_if_empty'      => false,
); ?>



<div id="thematiques">
    <?php wp_dropdown_categories($args ); ?>

<script type="text/javascript"><!--
    var dropdown = document.getElementById("cat");
    function onCatChange() {
        if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
            location.href = "<?php echo get_option('home');
?>/?cat="+dropdown.options[dropdown.selectedIndex].value;
        }
    }
    dropdown.onchange = onCatChange;
--></script>
</div>




            <?php
            /* Start the Loop */
            $n = 1;
            while ( have_posts() ) : the_post();
            if ($n == 1):
                if ($which_page == 1): // the first page of the archives starts
                                        // with bigger previews
                ?>
                <div id="prod-finies" class="listing">
                <?php
                else:
                ?>
                <div id="prod-finies" class="detail">
            <?php
                endif;
            elseif ($n == $number_of_highlighted_posts && $which_page == 1):
                ?>
                </div>
                <div id="prod-finies" class="detail">
            <?php
            endif;
           ?>

<div id='post<?php echo $i; ?>' class="production-item">
    <?php if ($n < $number_of_highlighted_posts && $which_page == 1) {  the_post_thumbnail( 'thumbnail' ); } ?>

<div class="caption">
<div class="artiste-title">
                    <?php
                    $artists = get_post_meta($post->ID, 'wpcf-artiste', false);
                    if (!empty($artists)):
                        echo "<ul class='artist'>";
                        foreach($artists as $key => $val) {
                            echo "<li><a href='/?s=". str_replace(" ", "+", $val)  ."'>" . $val . "</a></li>";
                        }
                        echo "</ul>";
                    endif;
                    ?>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
</div>

                    <?php
                    $annee = get_post_meta($post->ID, 'wpcf-annee', true);
                    $duree = get_post_meta($post->ID, 'wpcf-duree', true);
                    $genre = get_post_meta($post->ID, 'wpcf-genre', true);
                    $audio = get_post_meta($post->ID, 'wpcf-audio', false);

                    // embed a player if we have sound:
                    if($audio[0] != ''):
                        echo "<div class='clip' id='clip". $i . "'>";
                        $get_artists = get_post_meta($post->ID, 'wpcf-artiste', false);
                        $audio_artists = "";
                        if (!empty($artists)):
                            foreach($artists as $key => $val) {
                                $audio_artists .= $val;
                            }
                        endif;

                        $audio_title = the_title('', '', false);

                        $parse = explode(' --- ', $audio[0]);
                        if (count($parse)!=1) { // if there are several tracks
                            $url = $parse[1];
                        } else { // if there's only one track
                            $url = $parse[0];
                        }

                        $args = array( "duree" => $duree, "genre" => $genre, "annee" => $annee, "artiste" => $audio_artists );
                        $url = $url . '&' . http_build_query($args);

                        echo "<a class='audio mini-launcher' href='" . $url . "' data-link='".$post->ID ."' style='text-decoration: none;' title='". urlencode($audio_title) ."'>";
                        echo "<img src='" . get_template_directory_uri() . "/images/petit-play.png' alt='&#9654;' />";
                        echo "</a>";
                        echo "</div>";
                        $i++;
                    endif;
?>
<div class="details">
<?php
                    if ($genre) {
                        echo '<p class="genre">' . $genre . "</p>";
                    }

                    if ($duree) {
                        echo '<p class="duree">' . $duree . "</p>";
                    };

                    if ($annee) {
                        echo '<p class="annee">' . $annee . "</p>";
                    }
?>
</div>
</div> <!-- .caption -->
</div> <!-- .production-item -->


            <?php
            $n++;
            endwhile;
            ?>

</div><!-- #prod-finies -->
</article>

        <?php else : ?>
            <?php get_template_part( 'content', 'none' ); ?>
        <?php endif; ?>

        </div><!-- #content -->
    </section><!-- #primary -->


        <?php /* Display navigation to next/previous pages when applicable */ ?>
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
            <div id="nav-below" class="navigation">
                <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> précédent', 'acsr' ) ); ?></div>
                    <div class="nav-next"><?php previous_posts_link( __( 'suivant <span class="meta-nav">&rarr;</span>', 'acsr' ) ); ?></div>
            </div><!-- #nav-below -->
        <?php endif; ?>


<?php get_footer(); ?>
